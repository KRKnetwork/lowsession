const Session = require('express-session')
const fNoop = () => {}

module.exports = class LowSession extends Session.Store {
  constructor (lowdb) {
    super()
    this.lowdb = lowdb
  }

  get (sID, fDone = fNoop) {
    try {
      this._clean()
      fDone(null, this.lowdb.find({ id: sID }).cloneDeep().value())
    } catch (e) {
      fDone(e, null)
    }
  }

  set (sID, jSessionIn, fDone = fNoop) {
    try {
      this._clean()

      let jSession = JSON.parse(JSON.stringify(jSessionIn))
      jSession.id = sID

      const bFound = this.lowdb.find({ id: sID }).value()

      if (bFound !== undefined) {
        this.lowdb.find({ id: sID }).assign(jSession).write()
      } else {
        this.lowdb.push(jSession).write()
      }

      fDone(null)
    } catch (e) {
      fDone(e, null)
    }
  }

  destroy (sID, fDone = fNoop) {
    try {
      this._clean()

      fDone(null, this.lowdb.remove({ id: sID }).write())
    } catch (e) {
      fDone(e, null)
    }
  }

  _clean () {
    const dNow = new Date()

    let aToKill = this.lowdb.filter(d => dNow > new Date(d.cookie.expires)).value()

    for (let i = aToKill.length - 1; i >= 0; i--) {
      this.lowdb.remove({ id: aToKill[i].id }).value()
    }

    this.lowdb.write()
  }

  // touch (sID, jSession, fDone) {
  //     var store = this;
  //     var psid = store.prefix + sid;
  //     if (!fn) fn = noop;
  //     if (store.disableTTL) return fn();

  //     var ttl = getTTL(store, sess);

  //     debug('EXPIRE "%s" ttl:%s', sid, ttl);
  //     store.client.expire(psid, ttl, function (er) {
  //       if (er) return fn(er);
  //       debug('EXPIRE complete');
  //       fn.apply(this, arguments);
  //     });
  //   }
}
