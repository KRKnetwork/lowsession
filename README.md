# LowSession

Express Session Stores Implementation for LowDB

## Installing

Installing

```shell
npm i @krknet/lowsession
```

# Usage

```js
const LowSession = require('@krknet/lowsession')

Session({
  store: new LowSession(localdb.get('sessions')),
  secret: 'session',
  resave: true,
  unset: 'destroy',
  saveUninitialized: false,
  cookie: { maxAge: 86400000 }
})
```

## Developing

Issue List: [https://gitlab.com/KRKnetwork/lowsession/issues](https://gitlab.com/KRKnetwork/lowsession/issues)
